$(function () { 
   loadElementAfter('/data/formulario.html', '.navbar');
   loadElementAfter('/data/produtos.html', '.container');

    loadElementIn('/data/icone-face.html','#footer');
    loadElementIn('/data/icone-insta.html','#footer');
    loadElementIn('/data/icone-you.html','#footer');
    



    function loadElementIn(element, local) {
        $.ajax({url: element, success: function (result) { 
            $(local).append(result)
        }});
    }
    function loadElementAfter(element, local) {
        $.ajax({url: element, success: function (result) { 
            $(local).after(result)
        }});
    }
});